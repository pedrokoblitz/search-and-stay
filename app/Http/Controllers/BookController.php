<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use App\Http\Requests\BookRequest;

class BookController extends Controller
{
    public function __construct()
    {
        this->middleware('auth');
    }

    /**
     * @OA\Get(
     *     path="/books",
     *     tags={"Books"},
     *     summary="Get list of books",
     *     description="Returns list of books",
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad request"
     *     ),
     * )
     */
    public function index()
    {
        try {
            $books = Book::all();
            return $this->sendResponse($books, 'Books retrieved successfully.');
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    /**
     * @OA\Get(
     *     path="/books/{id}",
     *     tags={"Books"},
     *     summary="Get book information",
     *     description="Returns book data",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of book that needs to be fetched",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad request"
     *     ),
     * )
     */
    public function show(int $id)
    {
        try {
            $book = Book::find($id);
            return $this->sendResponse($book, 'Book retrieved successfully.');
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    /**
     * @OA\Post(
     *     path="/books",
     *     tags={"Books"},
     *     summary="Create new book",
     *     description="Create new book with provided data",
     *     @OA\RequestBody(
     *         required=true,
     *         description="Pass book data",
     *         @OA\JsonContent(ref="#/components/schemas/BookRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad request"
     *     ),
     * )
     */
    public function store(BookRequest $request)
    {
        $input = $request->all();
        try {
            $book = Book::create($input);
            $msg = 'Book saved successfully.';
            return $this->sendSuccess($msg);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    /**
     * @OA\Put(
     *     path="/books/{id}",
     *     tags={"Books"},
     *     summary="Update an existing book",
     *     description="Update book data",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of book that needs to be updated",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="Updated book data",
     *         @OA\JsonContent(ref="#/components/schemas/BookRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad request"
     *     ),
     * )
     */
    public function update(BookRequest $request, int $id)
    {
        try {
            $book = Book::find($id);
            if (empty($book)) {
                $msg = 'Book not found';
                return $this->sendError($msg);
            }
            $book->fill($request->all());
            $book->save();
            $msg = 'Book saved successfully.';
            return $this->sendSuccess($msg);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }

    /**
     * @OA\Delete(
     *     path="/books/{id}",
     *     tags={"Books"},
     *     summary="Delete a book",
     *     description="Delete a book",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of book that needs to be deleted",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad request"
     *     ),
     * )
     */
    public function destroy(int $id)
    {
        try {
            $book = Book::find($id);
            if (empty($book)) {
                $msg = 'Book not found';
                return $this->sendError($msg);
            }
            $msg = 'Book deleted successfully.';
            $book->delete();
            return $this->sendSuccess($msg);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
    }
}
