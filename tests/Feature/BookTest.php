<?php

namespace Tests\Feature;

use App\Models\Book;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BookTest extends TestCase
{
    use RefreshDatabase;

    public function test_can_get_all_books()
    {
        $response = $this->get('/books');
        $response->assertStatus(200);
    }

    public function test_can_get_single_book()
    {
        $book = Book::factory()->create();
        $response = $this->get("/books/{$book->id}");
        $response->assertStatus(200);
    }

    public function test_can_store_book()
    {
        $data = [
            'title' => 'Sample Title',
            'isbn' => '978-3-16-148410-0',
            'value' => 60.30,
        ];

        $response = $this->post('/books', $data);
        $response->assertStatus(200);
    }

    public function test_can_update_book()
    {
        $book = Book::factory()->create();
        $data = [
            'title' => 'Updated Title',
            'isbn' => '978-4-13-148410-0',
            'value' => 40.30,
        ];

        $response = $this->put("/books/{$book->id}", $data);
        $response->assertStatus(200);
    }

    public function test_can_delete_book()
    {
        $book = Book::factory()->create();

        $response = $this->delete("/books/{$book->id}");
        $response->assertStatus(200);
    }
}
